#include <dht.h>
#include <simtronyx_RGB_LED.h>
#include <IRLibDecodeBase.h> 
#include <IRLib_P01_NEC.h>   
#include <IRLib_HashRaw.h>  //Must be last protocol
#include <IRLibCombo.h>     // After all protocols, include this
#include <IRLibRecvPCI.h> 
#include <Thread.h>
#include <ThreadController.h>

//Settings
static const uint8_t         NO_MOTION_LIMIT_MICROWAVE           =   40;
static const uint8_t         NO_MOTION_LIMIT_PIR                 =   40;
static const unsigned long   LED_MANUAL_STATE_TIMEOUT            =   10000;
static const unsigned long   WATER_RELAY_MANUAL_STATE_TIMEOUT    =   10000;
static const uint8_t         NO_WATER_LEAKAGE_LIMIT              =   40;
static const unsigned long   DHT_UPDATE_INTERVAL                 =   6000;
static const float           FAN_ENABLING_HUMIDITY               =   60;
//static const float           FAN_ENABLING_TEMPERATURE            =   30;
//static const float           FAN_ENABLING_HINDEX                 =   80;

//Used pins global
#define RED_PIN           9
#define GREEN_PIN        10
#define BLUE_PIN         11
#define IR_PIN            2
#define DHT_PIN           5
#define MICROWAVE_PIN     7
#define PIR_PIN           8
#define WATER_RELAY_PIN  12
#define WATER_LEAK1_PIN   6
#define SPEAKER_PIN       3
#define WATER_METER_PIN  13   //PULLUP -> Connect sensor to the GND.

#define __DEBUG

#define ARRAY_SIZE(A) (sizeof(A) / sizeof((A)[0]))
template< typename T, size_t N > size_t ArraySize (T (&) [N]){ return N; }

ThreadController threadController = ThreadController();

bool LED_MANUAL_STATE = false;
bool WATER_RELAY_MANUAL_STATE = false;

//Remote IR codes
const uint32_t GREEN_BUTTON                 = 0xFFB04F;
const uint32_t TRANSITION_BUTTON            = 0xFF906F; //"Переход" button
const uint32_t PROGRAM_BUTTON               = 0xFF38C7; //"Программа" button
const uint32_t UP_BUTTON                    = 0xFF08F7; //Синяя кнопка вверх
const uint32_t DOWN_BUTTON                  = 0xFF8877; //Синяя кнопка вниз
const uint32_t RESUME_BUTTON                = 0xFFB847; //"Возобновление воспроизведения"
const uint32_t SLOWDOWN_BUTTON              = 0xFF58A7; //"Замедлить воспроизведение"
const uint32_t LEFT_RIGHT_CH_BUTTON         = 0xFFD827; //"Левый/правый канал"
const uint32_t NUM1_BUTTON                  = 0xFF00FF; //1
const uint32_t NUM2_BUTTON                  = 0xFF807F; //2
const uint32_t NUM3_BUTTON                  = 0xFF40BF; //3
const uint32_t NUM4_BUTTON                  = 0xFFC03F; //4
const uint32_t NUM5_BUTTON                  = 0xFF20DF; //5
const uint32_t NUM6_BUTTON                  = 0xFFA05F; //6
const uint32_t NUM7_BUTTON                  = 0xFF609F; //7
const uint32_t NUM8_BUTTON                  = 0xFFE01F; //8
const uint32_t NUM9_BUTTON                  = 0xFF10EF; //9
const uint32_t NUM10_BUTTON                 = 0xFF50AF; //10
const uint32_t NUM10P_BUTTON                = 0xFFD02F; //10+
const uint32_t RIGTH_AR_BUTTON              = 0xFFC837; // >
const uint32_t LEFT_AR_BUTTON               = 0xFF48B7; // <
const uint32_t SHARP_BUTTON                 = 0xFF8A75; // #
const uint32_t B_BUTTON                     = 0xFFCA35; // b

//Serial outgoing commands
const char OUT_CMD_ALERT_ON[]      PROGMEM  = {"_ALERT_ON"};
const char OUT_CMD_ALERT_OFF[]     PROGMEM  = {"_ALERT_OFF"};
const char OUT_CMD_PRESENCE_ON[]   PROGMEM  = {"_PRESENCE_ON"};
const char OUT_CMD_PRESENCE_OFF[]  PROGMEM  = {"_PRESENCE_OFF"};
const char OUT_CMD_DHT_ERROR[]     PROGMEM  = {"_DHT_ERROR"};
const char OUT_CMD_WATER_METER[]   PROGMEM  = {"_WATER_METER"};
const char OUT_CMD_ENABLE_FAN[]    PROGMEM  = {"_ENABLE_FAN!"};
const char OUT_CMD_DISABLE_FAN[]   PROGMEM  = {"_DISABLE_FAN!"};

//Info outgoing messages:
// _MWAVE_MOTION_ON
// _MWAVE_MOTION_OFF
// _PIR_MOTION_ON
// _PIR_MOTION_OFF
// _BOXLEAK_ON
// _BOXLEAK_OFF
// _DHT_H_   + value = "_DHT_H_28.05" = temperature
// _DHT_T_   + value = "_DHT_T_30.00" = humidity
// _DHT_I_   + value = "_DHT_I_1030.22", "_DHT_I_30.03" = heat index

//Serial ingoing commands
const char IN_CMD_WATER_ON[]   PROGMEM = {"_WATER_ON!"};
const char IN_CMD_WATER_OFF[]  PROGMEM = {"_WATER_OFF!"};
const char IN_CMD_ALERT_ON[]   PROGMEM = {"_ALERT_ON!"};
const char IN_CMD_ALERT_OFF[]  PROGMEM = {"_ALERT_OFF!"};

//RGB Led Strip------------------------------------------------------
typedef struct ColorRGB {
  uint8_t R;
  uint8_t G;
  uint8_t B;
} ColorRGB;

const uint8_t defaultTime = 200;

ColorRGB RED_COLOR        =  {255,   0,   0};
ColorRGB BLUE_COLOR       =  {  0,   0, 255};
ColorRGB GREEN_COLOR      =  {  0, 255,   0};
ColorRGB WHITE_COLOR      =  {255, 255, 255};
ColorRGB BLACK_COLOR      =  {  0,   0,   0};
ColorRGB FUCHSIA_COLOR    =  {255,   0, 255};
ColorRGB AQUA_COLOR       =  {  0, 255, 255};
ColorRGB YELLOW_COLOR     =  {255, 255,   0};
ColorRGB CHARTREUSE_COLOR =  {127, 255,   0};
ColorRGB PINK_COLOR       =  {255,  20, 147};
ColorRGB ORANGE_COLOR     =  {230, 140,   0};

const ColorRGB MAIN_COLORS[]  = { RED_COLOR, GREEN_COLOR, BLUE_COLOR, WHITE_COLOR, FUCHSIA_COLOR, 
                                  AQUA_COLOR, YELLOW_COLOR, CHARTREUSE_COLOR, PINK_COLOR
                                };

simtronyx_RGB_LED strip(RED_PIN,GREEN_PIN,BLUE_PIN);
bool isStripOn = false;
bool isStripAnimated = true;
unsigned long animateSpeed = 18;
uint8_t brightness = 255;
ColorRGB *currentStaticColor = NULL;

void turnLedOn(){
  isStripOn = true;
  strip.animateStart();
  currentStaticColor = NULL;
}

void turnLedOff(){
  isStripOn = false;
  strip.animateStop();
  strip.setRGB(0,0,0);
}

Thread *manualLedStateCancelTimer = NULL;

void manualLedStateCancelCallback(){
  Serial.println(F("Manual led state disabled"));
  if (manualLedStateCancelTimer) {
    threadController.remove(manualLedStateCancelTimer);
    delete manualLedStateCancelTimer;
    manualLedStateCancelTimer = NULL;
  }
  LED_MANUAL_STATE = false;
  if (isAnybodyPresent()) {
      if (!isStripOn) 
        turnLedOn();
  } else if (isStripOn)
    turnLedOff();
}

void turnLedOffOn(uint32_t ir_code){
  if (isStripOn) 
    turnLedOff();
  else
    turnLedOn();
  LED_MANUAL_STATE = true;
  if (manualLedStateCancelTimer) {
    threadController.remove(manualLedStateCancelTimer);
    delete manualLedStateCancelTimer;
    manualLedStateCancelTimer = NULL;
  }
  manualLedStateCancelTimer = new Thread(manualLedStateCancelCallback, LED_MANUAL_STATE_TIMEOUT);
  threadController.add(manualLedStateCancelTimer);
}

void ledBlink() {
  uint8_t oldRed = analogRead(RED_PIN);
  uint8_t oldGreen = analogRead(GREEN_PIN);
  uint8_t oldBlue = analogRead(BLUE_PIN);
  analogWrite(RED_PIN, 255);
  analogWrite(GREEN_PIN, 255);
  analogWrite(BLUE_PIN, 255);
  delay(2);
  analogWrite(RED_PIN, 0);
  analogWrite(GREEN_PIN, 0);
  analogWrite(BLUE_PIN, 0);
  delay(2);
  if(isStripOn) {
    analogWrite(RED_PIN, oldRed);
    analogWrite(GREEN_PIN, oldGreen);
    analogWrite(BLUE_PIN, oldBlue);
  } 
}

void randomColorsAnimation(uint32_t ir_code){
  randomSeed(analogRead(0));
  uint8_t minColorsCount = ir_code == RIGTH_AR_BUTTON || ir_code == LEFT_AR_BUTTON ? 1 : 2;
  uint8_t colorCount = random(minColorsCount,4);
  strip.animateColorsClear();
  for (int i=0; i<colorCount; i++) {
    uint8_t colorIndex = random(0,ARRAY_SIZE(MAIN_COLORS));
    strip.animateColorAdd(MAIN_COLORS[colorIndex].R, MAIN_COLORS[colorIndex].G, MAIN_COLORS[colorIndex].B, defaultTime);
  }
  turnLedOn();
  isStripAnimated = true;
  currentStaticColor = NULL;
}

void pausePlayAnimation(uint32_t ir_code){
    turnLedOn();
  if (isStripAnimated)
    strip.animateStop();
  else
    strip.animateStart();
  isStripAnimated = !isStripAnimated;
}

void increaseAnimateSpeed(uint32_t ir_code) {
  if (animateSpeed > 1 && isStripAnimated) {
    strip.animateSpeedSet(animateSpeed--);
  }
  #ifdef __DEBUG
    Serial.println(animateSpeed);
  #endif
}

void decreaseAnimateSpeed(uint32_t ir_code) {
  if (animateSpeed < 50 && isStripAnimated) {
    strip.animateSpeedSet(animateSpeed++);
  }
  #ifdef __DEBUG
    Serial.println(animateSpeed);
  #endif
}

void increaseBrightness(uint32_t ir_code){
  if (brightness < 240) {
    brightness+=15;
    strip.setBrightnessRGB(brightness,brightness,brightness);
    if (currentStaticColor)
      setStaticColor(*currentStaticColor);
  }
}

void decreaseBrightness(uint32_t ir_code){
  if (brightness > 15) {
    brightness-=15;
    strip.setBrightnessRGB(brightness,brightness,brightness);
    if (currentStaticColor)
      setStaticColor(*currentStaticColor);
  }
}

void setStaticColor(ColorRGB &color) {
  if (!isStripOn)
    turnLedOn();
  isStripAnimated = false;
  strip.animateStop();
  strip.setRGB(color.R, color.G, color.B);
  currentStaticColor = &color;
}

void setSlowRainbowAnimation(uint32_t ir_code){
  strip.animateColorsClear();
  randomSeed(analogRead(0));
  uint8_t firstColor = random(0,3);
  for (int i=0; i < 3; i++) {
    ColorRGB color = MAIN_COLORS[(firstColor + i) % 3];
    strip.animateColorAdd(color.R, color.G, color.B, defaultTime*5);
  }
  turnLedOn();
  isStripAnimated = true;
  currentStaticColor = NULL;
}

void rgbNodeSetup() {
  setSlowRainbowAnimation(0);
  strip.animateSpeedSet(animateSpeed);

}

void rgbNodeLoop() {
  if (isStripOn)
    strip.loop();
}

//-------------------------------------------------------------------
//DHT22 Humidity Sensor ----------------------------------------------
Thread dhtThread = Thread(dhtThreadCallback, DHT_UPDATE_INTERVAL);
dht DHT22Sensor;

#ifdef __DEBUG
  struct {
    uint32_t total;
    uint32_t ok;
    uint32_t crc_error;
    uint32_t time_out;
    uint32_t connect;
    uint32_t ack_l;
    uint32_t ack_h;
    uint32_t unknown;
  } stat = { 0,0,0,0,0,0,0,0};
#endif

void dhtThreadCallback(){
  #ifdef __DEBUG
    uint32_t start = micros();
  #endif
  int chk = DHT22Sensor.read22(DHT_PIN);
  #ifdef __DEBUG
    uint32_t stop = micros();
  #endif
  
  #ifdef __DEBUG
    stat.total++;
    switch (chk)
    {
    case DHTLIB_OK:
        stat.ok++;
        Serial.print(F("OK,\t"));
        break;
    case DHTLIB_ERROR_CHECKSUM:
        stat.crc_error++;
        Serial.print(F("Checksum error,\t"));
        break;
    case DHTLIB_ERROR_TIMEOUT:
        stat.time_out++;
        Serial.print(F("Time out error,\t"));
        break;
    case DHTLIB_ERROR_CONNECT:
        stat.connect++;
        Serial.print(F("Connect error,\t"));
        break;
    case DHTLIB_ERROR_ACK_L:
        stat.ack_l++;
        Serial.print(F("Ack Low error,\t"));
        break;
    case DHTLIB_ERROR_ACK_H:
        stat.ack_h++;
        Serial.print(F("Ack High error,\t"));
        break;
    default:
        stat.unknown++;
        Serial.print(F("Unknown error,\t"));
        break;
    }
  
    // DISPLAY DATA
    Serial.print(DHT22Sensor.humidity, 1);
    Serial.print(F(",\t"));
    Serial.print(DHT22Sensor.temperature, 1);
    Serial.print(F(",\t"));
    Serial.print(stop - start);
    Serial.println();
  
    if (stat.total % 20 == 0)
    {
        Serial.println(F("\nTOT\tOK\tCRC\tTO\tCON\tACK_L\tACK_H\tUNK"));
        Serial.print(stat.total);
        Serial.print(F("\t"));
        Serial.print(stat.ok);
        Serial.print(F("\t"));
        Serial.print(stat.crc_error);
        Serial.print(F("\t"));
        Serial.print(stat.time_out);
        Serial.print(F("\t"));
        Serial.print(stat.connect);
        Serial.print(F("\t"));
        Serial.print(stat.ack_l);
        Serial.print(F("\t"));
        Serial.print(stat.ack_h);
        Serial.print(F("\t"));
        Serial.print(stat.unknown);
        Serial.println(F("\n"));
    }
  #endif
  if (chk == DHTLIB_OK) {
    Serial.print("_DHT_T_");Serial.println(DHT22Sensor.temperature, 1);
    Serial.print("_DHT_H_");Serial.println(DHT22Sensor.humidity, 1);
    float heatIndex = computeHeatIndex(DHT22Sensor.temperature, DHT22Sensor.humidity);
    Serial.print("_DHT_I_");Serial.println(heatIndex, 1);
  } else {
    sendOutSerialCommand(OUT_CMD_DHT_ERROR);
  }
}

void dht22NodeSetup() {

  threadController.add(&dhtThread);
}

//-------------------------------------------------------------------
// MISC -------------------------------------------------------------

float computeHeatIndex(float temperature, float percentHumidity) {
  // Using both Rothfusz and Steadman's equations
  // http://www.wpc.ncep.noaa.gov/html/heatindex_equation.shtml
  float hi;

  temperature = temperature * 1.8 + 32;

  hi = 0.5 * (temperature + 61.0 + ((temperature - 68.0) * 1.2) + (percentHumidity * 0.094));

  if (hi > 79) {
    hi = -42.379 +
             2.04901523 * temperature +
            10.14333127 * percentHumidity +
            -0.22475541 * temperature*percentHumidity +
            -0.00683783 * pow(temperature, 2) +
            -0.05481717 * pow(percentHumidity, 2) +
             0.00122874 * pow(temperature, 2) * percentHumidity +
             0.00085282 * temperature*pow(percentHumidity, 2) +
            -0.00000199 * pow(temperature, 2) * pow(percentHumidity, 2);

    if((percentHumidity < 13) && (temperature >= 80.0) && (temperature <= 112.0))
      hi -= ((13.0 - percentHumidity) * 0.25) * sqrt((17.0 - abs(temperature - 95.0)) * 0.05882);

    else if((percentHumidity > 85.0) && (temperature >= 80.0) && (temperature <= 87.0))
      hi += ((percentHumidity - 85.0) * 0.1) * ((87.0 - temperature) * 0.2);
  }

  return  (hi - 32) * 0.55555;
}

//-------------------------------------------------------------------
//Water Relay -------------------------------------------------------

bool isWaterRelayOn = true;

Thread *manualWaterRelayStateCancelTimer = NULL;

void setWaterRelayState (bool state) {
  pinMode(WATER_RELAY_PIN, OUTPUT);
  digitalWrite(WATER_RELAY_PIN, state);
  isWaterRelayOn = state;
}

void turnWaterRelayOff(uint32_t ir_code) {  
  if (ir_code > 0) {
    renewManualStateWaterTimer();
    setWaterRelayState(0);
  } else if(!WATER_RELAY_MANUAL_STATE) {
    setWaterRelayState(0);
  }
}

void turnWaterRelayOn(uint32_t ir_code) {
  if (ir_code > 0) {
    renewManualStateWaterTimer();
    setWaterRelayState(1);
  } else if(!WATER_RELAY_MANUAL_STATE) {
    setWaterRelayState(1);
  }
}

void renewManualStateWaterTimer(){
  WATER_RELAY_MANUAL_STATE = true;
  if(manualWaterRelayStateCancelTimer) {
    threadController.remove(manualWaterRelayStateCancelTimer);
    delete manualWaterRelayStateCancelTimer;
    manualWaterRelayStateCancelTimer = NULL;
  }
  manualWaterRelayStateCancelTimer = new Thread(manualWaterStateCancelCallback, WATER_RELAY_MANUAL_STATE_TIMEOUT);
  threadController.add(manualWaterRelayStateCancelTimer);
}

void manualWaterStateCancelCallback(){
  Serial.println(F("Manual water state disabled"));
  if(manualWaterRelayStateCancelTimer) {
    threadController.remove(manualWaterRelayStateCancelTimer);
    delete manualWaterRelayStateCancelTimer;
    manualWaterRelayStateCancelTimer = NULL;
  }
  WATER_RELAY_MANUAL_STATE = false;
  if (!isWaterLeakageDetected()) {
      if (!isWaterRelayOn) 
        turnWaterRelayOn(0);
  } else if (isWaterRelayOn)
    turnWaterRelayOff(0);
}

void waterRelayNodeSetup(){
  turnWaterRelayOn(0);
}

//-------------------------------------------------------------------
//IR Remote  --------------------------------------------------------

IRdecode myDecoder;
IRrecvPCI myReceiver(IR_PIN); 

typedef struct IrCode {
    uint32_t value;
    void (*function)(uint32_t);
    bool repeatable;
} IrCode;

//Functions to handle IR codes

const IrCode codes[]  = {
                          {GREEN_BUTTON,             turnLedOffOn,              0}, //On/Off led
                          {TRANSITION_BUTTON,        randomColorsAnimation,     0}, //Random animation
                          {PROGRAM_BUTTON,           randomColorsAnimation,     0}, //Random animation
                          {LEFT_AR_BUTTON,           randomColorsAnimation,     0}, //Random animation
                          {RIGTH_AR_BUTTON,          randomColorsAnimation,     0}, //Random animation
                          {LEFT_RIGHT_CH_BUTTON,     increaseAnimateSpeed,      1}, //Increase speed
                          {SLOWDOWN_BUTTON,          decreaseAnimateSpeed,      1}, //Decrease speed
                          {UP_BUTTON,                increaseBrightness,        1}, //Increase brightness
                          {DOWN_BUTTON,              decreaseBrightness,        1}, //Decrease brightness
                          {NUM1_BUTTON,              handleStaticColorCode,     0}, //White color
                          {NUM2_BUTTON,              handleStaticColorCode,     0}, //Red color
                          {NUM3_BUTTON,              handleStaticColorCode,     0}, //Green color
                          {NUM4_BUTTON,              handleStaticColorCode,     0}, //Blue color
                          {NUM5_BUTTON,              handleStaticColorCode,     0}, //Fuchsia color
                          {NUM6_BUTTON,              handleStaticColorCode,     0}, //Aqua color
                          {NUM7_BUTTON,              handleStaticColorCode,     0}, //Yellow color
                          {NUM8_BUTTON,              handleStaticColorCode,     0}, //Chartreuse color
                          {NUM9_BUTTON,              handleStaticColorCode,     0}, //Pink color
                          {NUM10_BUTTON,             handleStaticColorCode,     0}, //Orange color
                          {NUM10P_BUTTON,            setSlowRainbowAnimation,   0}, //Slow rainbow
                          {RESUME_BUTTON,            pausePlayAnimation,        0}, //Play/Pause animation
                          {SHARP_BUTTON,             turnWaterRelayOff,         0}, // Turn water off
                          {B_BUTTON,                 turnWaterRelayOn,          0}, // Turn water on                          
                      }; 

const char string_0[]  PROGMEM = "On/Off led";
const char string_1[]  PROGMEM = "Random animation";
const char string_2[]  PROGMEM = "Random animation";
const char string_3[]  PROGMEM = "Random animation";
const char string_4[]  PROGMEM = "Random animation";
const char string_5[]  PROGMEM = "Increase speed";
const char string_6[]  PROGMEM = "Decrease speed";
const char string_7[]  PROGMEM = "Increase brightness";
const char string_8[]  PROGMEM = "Decrease brightness";
const char string_9[]  PROGMEM = "White color";
const char string_10[] PROGMEM = "Red color";
const char string_11[] PROGMEM = "Green color";
const char string_12[] PROGMEM = "Blue color";
const char string_13[] PROGMEM = "Fuchsia color";
const char string_14[] PROGMEM = "Aqua color";
const char string_15[] PROGMEM = "Yellow color";
const char string_16[] PROGMEM = "Chartreuse color";
const char string_17[] PROGMEM = "Pink color";
const char string_18[] PROGMEM = "Orange color";
const char string_19[] PROGMEM = "Slow rainbow";
const char string_20[] PROGMEM = "Play/Pause animation";
const char string_21[] PROGMEM = "Turn water off";
const char string_22[] PROGMEM = "Turn water on";

const char  * const codesDescription []  PROGMEM = {string_0, string_1, string_2, string_3, string_4, 
  string_5, string_6, string_7, string_8, string_9, string_10, string_11, string_12, string_13, 
  string_14, string_15, string_16, string_17, string_18, string_19, string_20, string_21, string_22};

char descriptionBuffer[30];
char  *stringFromProgmemArray(const char  * const stringArray [], uint8_t index) {
  strcpy_P(descriptionBuffer, (char*)pgm_read_word_near(&(stringArray[index])));
  return descriptionBuffer;
}

void handleIrCode (uint32_t code) {
  static int8_t lastCodeIndex = -1;
  if (code == 0xFFFFFFFF && lastCodeIndex > -1 && codes[lastCodeIndex].repeatable) {
    codes[lastCodeIndex].function(code);
    Serial.println(stringFromProgmemArray(codesDescription, lastCodeIndex));
  } else {
    Serial.print(F("_IRRAW_"));Serial.println(code, HEX);
    for (int i=0;i<ARRAY_SIZE(codes);i++) {
      if (codes[i].value == code) {
        lastCodeIndex = i;
        ledBlink();
        codes[i].function(code);
        Serial.println(stringFromProgmemArray(codesDescription, i));
        return;
      }
    }
    lastCodeIndex = -1;
  }
}

void handleStaticColorCode (uint32_t code){
  switch (code) {
    case NUM1_BUTTON:  setStaticColor(WHITE_COLOR);      break;
    case NUM2_BUTTON:  setStaticColor(RED_COLOR);        break;
    case NUM3_BUTTON:  setStaticColor(GREEN_COLOR);      break;
    case NUM4_BUTTON:  setStaticColor(BLUE_COLOR);       break;
    case NUM5_BUTTON:  setStaticColor(FUCHSIA_COLOR);    break;
    case NUM6_BUTTON:  setStaticColor(AQUA_COLOR);       break;
    case NUM7_BUTTON:  setStaticColor(YELLOW_COLOR);     break;
    case NUM8_BUTTON:  setStaticColor(CHARTREUSE_COLOR); break;
    case NUM9_BUTTON:  setStaticColor(PINK_COLOR);       break;
    case NUM10_BUTTON: setStaticColor(ORANGE_COLOR);     break;
    default: 
      #ifdef __DEBUG
        Serial.println(F("ERROR: Unknown color code"));
      #endif
      break;
  }
}


void ir_remote_setup() {
  myReceiver.enableIRIn(); // Start the receiver
}

void ir_remote_loop() {
  if(myReceiver.getResults()) {
    myDecoder.decode();
    if(myDecoder.protocolNum==UNKNOWN) {
    } else {
      //myDecoder.dumpResults(false);
      handleIrCode(myDecoder.value);
    };
    myReceiver.enableIRIn(); 
  }
}

//-----------------------------------------------------------------
//Motion Sensors----------------------------------------------------------

class ThresholdSensorThread: public Thread {

public:
  bool lastValue = false;
  uint8_t pin;
  char *name;
  bool isActive = false;
  uint8_t lowLevelLimit = 40;
  bool isInverted = false;
  
private:
  uint8_t lowLevelCount = 40;
  
  void run(){
    bool currentValue = digitalRead(pin) ^ isInverted;
    if (currentValue == false) { //No motion detected
      if (lowLevelCount > lowLevelLimit) {
        isActive = false;
      }
      else
        lowLevelCount++;
    } else { //Motion detected
      lowLevelCount = 0;
      if (lastValue == false) {
        isActive = true;
      }
    }
    if (lastValue != currentValue) {
      Serial.print(name); 
      Serial.println(currentValue ? F("_ON") : F("_OFF"));
    }
    lastValue = currentValue;    
    runned();
  }
};

Thread presenceMonitor = Thread();
ThresholdSensorThread microwaveSensor = ThresholdSensorThread();
ThresholdSensorThread pirSensor = ThresholdSensorThread();

bool isAnybodyPresent() {
  return microwaveSensor.isActive || pirSensor.isActive;
}

void presenceMonitorCallback(){
  static bool lastValue = false;
  bool currentValue = isAnybodyPresent();
  if(currentValue != lastValue) {
    sendOutSerialCommand(currentValue ? OUT_CMD_PRESENCE_ON : OUT_CMD_PRESENCE_OFF);
    lastValue = currentValue;
    if (LED_MANUAL_STATE)
      return;

    if (isAnybodyPresent())
      if (!isStripOn)
        turnLedOn();
    else if (isStripOn)
      turnLedOff();
  }  
}

void motionSensorsSetup(){
  microwaveSensor.setInterval(200);
  microwaveSensor.pin = MICROWAVE_PIN; 
  microwaveSensor.name = "_MWAVE";
  microwaveSensor.lowLevelLimit = NO_MOTION_LIMIT_MICROWAVE;
  pinMode(microwaveSensor.pin, INPUT);
  threadController.add(&microwaveSensor);

  pirSensor.setInterval(200);
  pirSensor.pin = PIR_PIN; 
  pirSensor.name = "_PIR";
  pirSensor.lowLevelLimit = NO_MOTION_LIMIT_PIR;
  pinMode(pirSensor.pin, INPUT);
  threadController.add(&pirSensor);

  presenceMonitor.onRun(presenceMonitorCallback);
  presenceMonitor.setInterval(200);
  threadController.add(&presenceMonitor);
}

//Water sensors--------

Thread leakageMonitor = Thread();
bool isExternalLeakageAlert = false;

ThresholdSensorThread leakageSensor1 = ThresholdSensorThread();

bool isWaterLeakageDetected(){
  if (leakageSensor1.isActive || isExternalLeakageAlert) 
    return true;
  return false;
}

void leakageMonitorCallback(){
  if (isWaterLeakageDetected()) {
    startAlert();
    if (isWaterRelayOn && !WATER_RELAY_MANUAL_STATE) {
      turnWaterRelayOff(0);
    }
  } else { 
    stopAlert();
    if (!isWaterRelayOn && !WATER_RELAY_MANUAL_STATE){
      turnWaterRelayOn(0);
    }
  }
}

void externalAlertOn(){
  isExternalLeakageAlert = true;
}

void externalAlertOff(){
  isExternalLeakageAlert = false;
}

void waterLeakageSensorsSetup(){
  leakageSensor1.setInterval(200);
  leakageSensor1.pin = WATER_LEAK1_PIN;
  leakageSensor1.name = "_BOXLEAK";
  leakageSensor1.lowLevelLimit = NO_WATER_LEAKAGE_LIMIT;
  leakageSensor1.isInverted = true;
  pinMode(leakageSensor1.pin, INPUT);
  threadController.add(&leakageSensor1);

  leakageMonitor.onRun(leakageMonitorCallback);
  leakageMonitor.setInterval(200);
  threadController.add(&leakageMonitor);
}

//-----------------------------------------------------------------
//Serial communication---------------------------------------------

const char  * const inCommands []  PROGMEM = { 
                                                IN_CMD_WATER_ON, 
                                                IN_CMD_WATER_OFF,
                                                IN_CMD_ALERT_ON,
                                                IN_CMD_ALERT_OFF
                                             };

void (*inCommandCallbacks[])(uint32_t)     = {
                                                turnWaterRelayOn, 
                                                turnWaterRelayOff,
                                                externalAlertOn,
                                                externalAlertOff
                                             };

String inputString = "";         // a string to hold incoming data
bool stringComplete = false;  // whether the string is complete

void serialEvent() {
  while (Serial.available()) {
    // get the new byte:
    char inChar = (char)Serial.read();
    // add it to the inputString:
    if (inChar == '\n') {
      stringComplete = true;
    } else {
      inputString += inChar;
    }
  }
}

char outCommandBuffer[30];

void sendOutSerialCommand(const char *message){
  memset(outCommandBuffer, 0, sizeof(outCommandBuffer));
  for (int k = 0; k < strlen_P(message); k++)
    outCommandBuffer[k] =  pgm_read_word(message + k);
  
  Serial.println(outCommandBuffer);
}

void serialCommunicationSetup(){
  inputString.reserve(200);
}

void serialCommunicationLoop(){
  if (stringComplete) {
    inputString.toUpperCase();
    inputString.trim();
    Serial.println(inputString);
    for (int i=0; i< ArraySize(inCommands); i++) {
      if(inputString.equals(stringFromProgmemArray(inCommands, i))) {
        inCommandCallbacks[i](1);
      }
    }    
    // clear the string:
    inputString = "";
    stringComplete = false;
  }
}

//-----------------------------------------------------------------
//Alert------------------------------------------------------------

Thread *alertThread = NULL;

void alertThreadCallback(){
  static uint8_t toneFreq = 0;
  static bool isGap = false;
  
  strip.setBrightnessRGB(255,255,255);
  if (isGap) {
    setStaticColor(RED_COLOR);
    analogWrite(SPEAKER_PIN, toneFreq);
    toneFreq+=20;
  } else {
    sendOutSerialCommand(OUT_CMD_ALERT_ON);
    setStaticColor(BLACK_COLOR);
    analogWrite(SPEAKER_PIN, 0);
  }
  isGap=!isGap;
}

void startAlert() {
  if (alertThread) {
    threadController.remove(alertThread);
    delete alertThread;
    alertThread = NULL;
  }
  alertThread = new Thread();
  alertThread->setInterval(100);
  alertThread->onRun(alertThreadCallback);
  threadController.add(alertThread);
  pinMode(SPEAKER_PIN, OUTPUT);
}

void stopAlert() {
  if (alertThread) {
    threadController.remove(alertThread);
    delete alertThread;
    alertThread = NULL;
  }
  analogWrite(SPEAKER_PIN, 0);
  setSlowRainbowAnimation(0);
}

//-----------------------------------------------------------------
//Water meter sensor-----------------------------------------------

Thread waterMeterThread = Thread (waterMeterThreadCallback, 500);

void waterMeterThreadCallback(){
  static bool lastValue = false;
  bool currentValue = digitalRead(WATER_METER_PIN) ^ 1;
  if (currentValue != lastValue && currentValue)
    sendOutSerialCommand(OUT_CMD_WATER_METER);
  lastValue = currentValue;
}
void waterMeterSensorSetup() {
  pinMode(WATER_METER_PIN, INPUT_PULLUP);
  threadController.add(&waterMeterThread);
}

//-----------------------------------------------------------------

void setup() {
  delay(1000);
  Serial.begin(74880);
  Serial.println(F("BATHROOM MANAGER STARTED"));
  serialCommunicationSetup();
  ir_remote_setup();
  dht22NodeSetup();
  rgbNodeSetup();
  motionSensorsSetup();
  waterRelayNodeSetup();
  waterLeakageSensorsSetup();
  waterMeterSensorSetup();
}

void loop() {
  serialCommunicationLoop();
  ir_remote_loop();
  rgbNodeLoop();
  threadController.run();
}
