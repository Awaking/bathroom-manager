# README #

Bathroom manager

### Features ###

* IR Remote control
* Motion detection
* Light control
* Water leakage sensor & water relay on/off
* RGB LED control by PWM
* Water consumption statistics
* Speakers on/off control

### Used libraries ###

* [IRLib2](https://github.com/cyborg5/IRLib2)
* [simtronyx_RGB_LED](http://blog.simtronyx.de/en/a-rgb-led-library-for-arduino/)
* [ArduinoThread](https://github.com/ivanseidel/ArduinoThread)
* [DHTlib](https://github.com/RobTillaart/Arduino/tree/master/libraries/DHTlib)