#include "simtronyx_RGB_LED.h"

#define REDPIN 14
#define GREENPIN 0
#define BLUEPIN 4

simtronyx_RGB_LED strip(REDPIN,GREENPIN,BLUEPIN);
 
void setup() {
  
  strip.animateColorAdd(255,0,0,100);
  strip.animateColorAdd(0,255,0,100);  
  strip.animateColorAdd(0,0,255,100);  
  strip.animateStart();
}

void loop(){

  strip.loop();
}
